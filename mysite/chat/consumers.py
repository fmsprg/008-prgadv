# chat/consumers.py
import json
from channels.generic.websocket import AsyncWebsocketConsumer
import logging
import colorlog
import pprint

class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):

        pp = pprint.PrettyPrinter(depth=6)
        #pp.pprint( subscope[0] )

        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        allscope = self.__dict__.get("scope")
        subscope = allscope['client']
        client_ip_addr = subscope[0]
        client_ip_port = subscope[1]
        print(client_ip_addr)

        subscope = allscope['server']
        server_ip_addr = subscope[0]
        server_ip_port = subscope[1]
        print(server_ip_addr)


        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']
        print("Got Some Message from Room Group")
        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message
        }))