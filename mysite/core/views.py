from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User, Group

from django.contrib.auth.decorators import user_passes_test,login_required

class HelloView(APIView):
	permission_classes = (IsAuthenticated,) 


	def is_member(self, user):
		return user.groups.filter(name='testgroup1').exists()

	def is_in_one_of_many_groups(self, user):
		return user.groups.filter(name__in=['testgroup1', 'testgroup2']).exists()

	def get(self, request):
		
		user_id = Token.objects.get(key=request.auth.key).user_id
		user = User.objects.get(id=user_id)    	
		print(user.groups.all())
		print(self.is_member(user))
		print(self.is_in_one_of_many_groups(user))

		content = {'message': 'Hello, World!'}
		return Response(content)